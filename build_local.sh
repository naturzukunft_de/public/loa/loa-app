#!/bin/sh
set echo on

export SLR_BUILD_HOME=`pwd`
export WORKSPACE=/home/fredy/ws/loa

export CURRENT_APP_HOME=$WORKSPACE/loa-app
export CURRENT_APP_NAME=loa-app-spring-boot
. build_docker_image.sh
docker run -p 8080:8080 loa-app-spring-boot 