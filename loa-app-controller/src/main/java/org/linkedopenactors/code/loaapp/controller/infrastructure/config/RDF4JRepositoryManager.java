package org.linkedopenactors.code.loaapp.controller.infrastructure.config;

import java.net.MalformedURLException;

import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.config.RepositoryConfigException;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.naturzukunft.rdf4j.loarepository.PublicationLoa;
import de.naturzukunft.rdf4j.loarepository.PublicationRepo;
import de.naturzukunft.rdf4j.ommapper.Converter;

@Configuration
public class RDF4JRepositoryManager {
	
	@Value("${app.rdf4jServer}")
	private String rdf4jServer;
	
	@Value("${app.repositoryID}")
	private String repositoryID;
	
	private RepositoryManager getRepositoryManager() throws MalformedURLException {
		RepositoryManager rm = new RemoteRepositoryManager(rdf4jServer);
		rm.init();
		return rm;
	}
	
	
//	@Bean
	public Repository getKvmRepo() {
//		String url = "http://localhost:8080/rdf4j-server/repositories/" + repositoryId;
//		Repository actorsRepository;
//		RepositoryImplConfig repositoryTypeSpec = new HTTPRepositoryConfig(url);
//		RepositoryConfig repConfig = new RepositoryConfig(repositoryId, repositoryTypeSpec);
//		getRepositoryManager().addRepositoryConfig(repConfig);
		Repository actorsRepository;
		try {
			actorsRepository = getRepositoryManager().getRepository(repositoryID);
		} catch (RepositoryConfigException | RepositoryException | MalformedURLException e) {
			throw new RuntimeException(e);
		}
		return actorsRepository;
	}
	
	@Bean
	public PublicationRepo getPublicationRepo() {
		return new PublicationRepo(getKvmRepo(), new Converter<>(PublicationLoa.class));
	}
}
