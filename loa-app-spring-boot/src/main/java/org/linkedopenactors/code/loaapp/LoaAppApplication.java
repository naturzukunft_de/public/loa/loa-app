package org.linkedopenactors.code.loaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoaAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoaAppApplication.class, args);
	}

}
